import { createLocalVue, shallowMount } from '@vue/test-utils'
import Employees from '@/components/Employees.vue'

import '@/libs/filters'

describe('Employees', () => {
  let wrapper
  let vm
  beforeEach(() => {
    const localVue = createLocalVue()

    wrapper = shallowMount(Employees, {
      localVue
    })
    vm = wrapper.vm
  })

  it('Comprueba el funcionamiento de incremento/decremento de la semana', () => {
    // Valor inicial
    expect(vm.week).toBe(0)
    // Botones previous y next
    let previousButton = wrapper.find('.header__weekSelector button:first-child')
    let nextButton = wrapper.find('.header__weekSelector button:last-child')

    // Simulamos click previous
    previousButton.trigger('click')
    expect(vm.week).toBe(-1)
    // Simulamos click next
    nextButton.trigger('click')
    nextButton.trigger('click')
    expect(vm.week).toBe(1)
  })
})