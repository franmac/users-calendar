// Devuelve una array de los diías comprencidos entre 2 fechas con el formato indicado
function dateRange (startDate, endDate, formatType = 'YYYY-MM-DD') {
  const days = endDate.diff(startDate, 'days')

  return [...Array(days + 1).keys()].map(day => startDate.clone().add(day, 'days').format(formatType))
}

export {
  dateRange
}